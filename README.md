# kubesetup-behind-pfsense



## Description

This effort is to create a kubernetes cluster in my home lab behind a pfsense appliance that provideds DNS and routing for testing purposes. 

Currently, the hosts are NUC's running CentOS Stream 9 and using CRI-O instead of Docker's containerd (aka: 'dockerless'). Note: Upstream Fedora 
CRI-O out-of-the-box but that currently is not downstream yet in CentOS Stream 9/RHEL9 so is using the CRI-O installer... which I consider less
ideal _but_ suitable in this circumstance. The main reason I'm not using upstream Fedora is that it has a 6 month life space and not suitable
for this implentation due to it's 'short life' and risk of breaking changes during upgrades.

## Status
This implementation currently wraps Kubespray with my own inventory and some prepatory tasks extending the Kubespray setup. The pipeline runs
in a container that is 'downgrading' ansible to a version that it accepts and bootstrapping the SSH keys to connect and control the hosts
designated in the inventory. This is still a work in progress preparing this to control PFSense which is setup with Letsencrypt and No-IP
for incoming connnections so traffic can be 'routed' into the cluster via PFSense.

## Installation
-- In progress --
## Usage
-- In progress --
## Support
When the goals of the project are completed, I recommend contributing and using community support or simply 'forking' the project.
## Roadmap
Currently, the Kubernetes cluster sets up with Kubespray. There is more configuration to do so the next phase of getting the requirements
for controlling PFsense and using MetalLB and Flannel to coordinate automation between the cluster and PFSense.

## Contributing
Contributions are welcome _but_ the project is not 'feature complete' yet. It's reached it's first milestone of simply bootstapping Kubespray and providing prepatory steps on the CentOS Stream 9 hosts.

## Authors and acknowledgment

### Maintainer
James Jones <jamjon3@gmail.com>

## License
This project uses the MIT license and is included in the LICENSE file of this project
## Project status
This project is 'in-progress' but is not feature complete yet. As those milestones are reached, this documentation will be updated to cover those milestones into a complete implementation.